/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 100.0, "KoPercent": 0.0};
    var dataset = [
        {
            "label" : "FAIL",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "PASS",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.425, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [0.46, 500, 1500, "Post Create Product"], "isController": false}, {"data": [0.4675, 500, 1500, "Get Profiles"], "isController": false}, {"data": [0.505, 500, 1500, "Post offers "], "isController": false}, {"data": [0.1975, 500, 1500, "Post Create User"], "isController": false}, {"data": [0.495, 500, 1500, "Get Kategori by id"], "isController": false}, {"data": [0.4425, 500, 1500, "Post Sign In"], "isController": false}, {"data": [0.4875, 500, 1500, "Get Kategori"], "isController": false}, {"data": [0.03, 500, 1500, "Put Update Offer "], "isController": false}, {"data": [0.555, 500, 1500, "Delete delete  Product"], "isController": false}, {"data": [0.545, 500, 1500, "Get Product By ID"], "isController": false}, {"data": [0.5175, 500, 1500, "Get List Offers"], "isController": false}, {"data": [0.0575, 500, 1500, "Put Update Profiles"], "isController": false}, {"data": [0.485, 500, 1500, "Put Update Offer -1"], "isController": false}, {"data": [0.515, 500, 1500, "Put Update Product"], "isController": false}, {"data": [0.505, 500, 1500, "Get List All Products "], "isController": false}, {"data": [0.535, 500, 1500, "Put Update Offer -0"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 3200, 0, 0.0, 1084.1581249999972, 32, 4150, 947.0, 1827.9, 2064.8999999999996, 2628.779999999995, 20.426009970446117, 85.67701975139312, 79.41508380049852], "isController": false}, "titles": ["Label", "#Samples", "FAIL", "Error %", "Average", "Min", "Max", "Median", "90th pct", "95th pct", "99th pct", "Transactions/s", "Received", "Sent"], "items": [{"data": ["Post Create Product", 200, 0, 0.0, 981.7, 351, 1887, 903.0, 1611.2000000000003, 1717.5, 1858.9, 1.3845621322256838, 2.7093068535825546, 23.81221064814815], "isController": false}, {"data": ["Get Profiles", 200, 0, 0.0, 1068.9349999999997, 166, 3119, 1060.5, 1623.9, 1944.7999999999981, 2994.560000000005, 1.3818739592761744, 1.8449096945367613, 1.013464202555085], "isController": false}, {"data": ["Post offers ", 200, 0, 0.0, 830.2500000000001, 355, 1574, 821.5, 1021.9, 1097.35, 1272.6700000000003, 1.3825999792610004, 3.820654369447997, 1.1566312951505306], "isController": false}, {"data": ["Post Create User", 200, 0, 0.0, 1648.7100000000005, 737, 3048, 1613.5, 2155.4, 2466.9499999999994, 3003.930000000001, 1.3713940906628632, 2.4998947347895255, 1.1339714887168553], "isController": false}, {"data": ["Get Kategori by id", 200, 0, 0.0, 962.2800000000003, 247, 2035, 915.5, 1491.6000000000001, 1609.6499999999999, 1960.860000000001, 1.3826573291208373, 0.9887755229901347, 1.0205334335183789], "isController": false}, {"data": ["Post Sign In", 200, 0, 0.0, 1061.0649999999991, 188, 2470, 991.0, 1681.9, 2192.4499999999994, 2274.8100000000004, 1.3719679508286686, 2.499152166680386, 1.1696428724549994], "isController": false}, {"data": ["Get Kategori", 200, 0, 0.0, 1025.4050000000004, 250, 2629, 977.0, 1665.7, 1816.35, 2427.6600000000003, 1.3678954927843514, 1.4293439231242733, 1.0069661146638396], "isController": false}, {"data": ["Put Update Offer ", 200, 0, 0.0, 1790.0100000000002, 920, 2316, 1780.0, 2044.6, 2121.3999999999996, 2300.59, 1.3809007615667699, 23.81195470602349, 2.102488836280406], "isController": false}, {"data": ["Delete delete  Product", 200, 0, 0.0, 806.9149999999997, 32, 1898, 746.0, 1201.7, 1297.9, 1831.89, 1.412489229769623, 1.5393097959659308, 1.0912858683983786], "isController": false}, {"data": ["Get Product By ID", 200, 0, 0.0, 748.4849999999999, 114, 1526, 726.5, 1038.5, 1202.3999999999996, 1479.2400000000016, 1.3930001741250218, 2.6667519153752397, 1.045539134598642], "isController": false}, {"data": ["Get List Offers", 200, 0, 0.0, 759.37, 260, 1308, 751.5, 958.0, 1044.4499999999996, 1304.8400000000001, 1.3817975804724365, 1.8282099589606118, 1.0294122092179716], "isController": false}, {"data": ["Put Update Profiles", 200, 0, 0.0, 2070.4749999999995, 1163, 4150, 2040.0, 2718.7, 2975.8, 4074.700000000001, 1.3653647913381257, 2.216464400673125, 23.224675096514225], "isController": false}, {"data": ["Put Update Offer -1", 200, 0, 0.0, 1059.8449999999998, 542, 1734, 1046.5, 1281.7, 1407.75, 1702.1300000000008, 1.3879154204342787, 22.129532031786734, 1.0009933354036404], "isController": false}, {"data": ["Put Update Product", 200, 0, 0.0, 875.3349999999997, 66, 1916, 841.0, 1253.7, 1359.9999999999995, 1885.4600000000005, 1.4051258992805755, 2.797380625772819, 24.20838445210628], "isController": false}, {"data": ["Get List All Products ", 200, 0, 0.0, 927.8400000000001, 428, 1782, 929.5, 1123.0, 1222.2999999999997, 1691.93, 1.382217768409413, 18.277501544196415, 1.0136083658730433], "isController": false}, {"data": ["Put Update Offer -0", 200, 0, 0.0, 729.9100000000001, 319, 1357, 700.0, 955.7, 1033.55, 1343.1400000000008, 1.3860974426502184, 1.8010197562201125, 1.1107188647861945], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Median
            case 8:
            // Percentile 1
            case 9:
            // Percentile 2
            case 10:
            // Percentile 3
            case 11:
            // Throughput
            case 12:
            // Kbytes/s
            case 13:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": []}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 3200, 0, "", "", "", "", "", "", "", "", "", ""], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
