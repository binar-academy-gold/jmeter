/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 75.0, "KoPercent": 25.0};
    var dataset = [
        {
            "label" : "FAIL",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "PASS",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.335, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [0.3925, 500, 1500, "Post Daftar "], "isController": false}, {"data": [0.7725, 500, 1500, "Get buyer Product by ID 3"], "isController": false}, {"data": [0.4575, 500, 1500, "PUT buyer Order by id order"], "isController": false}, {"data": [0.0, 500, 1500, "Post  buyer Order"], "isController": false}, {"data": [0.1525, 500, 1500, "Get Seller Product by ID"], "isController": false}, {"data": [0.375, 500, 1500, "Get buyer Product "], "isController": false}, {"data": [0.0, 500, 1500, "Post Seller Product "], "isController": false}, {"data": [0.56, 500, 1500, "Get buyer Order"], "isController": false}, {"data": [0.61, 500, 1500, "Post Login"], "isController": false}, {"data": [0.0875, 500, 1500, "Get Seller Product"], "isController": false}, {"data": [0.0, 500, 1500, "Delete Seller Product by ID manual"], "isController": false}, {"data": [0.6125, 500, 1500, "Get buyer Order by id order"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 2400, 600, 25.0, 1669.5420833333337, 32, 16156, 972.0, 3932.9, 4827.849999999999, 10053.439999999944, 3.882929670436344, 4.868197794819524, 7.128716653824686], "isController": false}, "titles": ["Label", "#Samples", "FAIL", "Error %", "Average", "Min", "Max", "Median", "90th pct", "95th pct", "99th pct", "Transactions/s", "Received", "Sent"], "items": [{"data": ["Post Daftar ", 200, 0, 0.0, 1526.6349999999993, 292, 11574, 876.5, 3299.900000000001, 3984.35, 6718.99, 0.3265023597958054, 0.18748377691399765, 0.5374550880699303], "isController": false}, {"data": ["Get buyer Product by ID 3", 200, 0, 0.0, 664.9450000000003, 32, 6947, 362.5, 1704.8000000000025, 2749.85, 4091.0700000000006, 0.3298843920148184, 0.30701154842785344, 0.11114269066905502], "isController": false}, {"data": ["PUT buyer Order by id order", 200, 0, 0.0, 1807.5699999999993, 69, 16156, 935.0, 3873.600000000001, 5969.099999999991, 12508.120000000003, 0.3309367993447451, 0.22105544018731021, 0.12668674349916023], "isController": false}, {"data": ["Post  buyer Order", 200, 200, 100.0, 1592.6399999999996, 69, 11836, 937.5, 4086.9, 4962.299999999996, 11733.990000000047, 0.3300760330142048, 0.10927321796075726, 0.13280402890805895], "isController": false}, {"data": ["Get Seller Product by ID", 200, 0, 0.0, 2956.1199999999985, 299, 12275, 2204.0, 5439.0, 7274.3499999999985, 11649.230000000001, 0.32648743594724616, 0.8496963054681748, 0.10999820840019524], "isController": false}, {"data": ["Get buyer Product ", 200, 0, 0.0, 1444.9450000000006, 236, 7779, 1085.0, 3146.500000000001, 3740.949999999999, 7206.050000000008, 0.3292023919845802, 0.24561584714474535, 0.1314880647672786], "isController": false}, {"data": ["Post Seller Product ", 200, 200, 100.0, 2284.265, 399, 10082, 1688.0, 3992.5, 5000.899999999997, 10053.440000000002, 0.3262264892647018, 0.10322010011890954, 5.467887283854561], "isController": false}, {"data": ["Get buyer Order", 200, 0, 0.0, 1356.5700000000004, 72, 10531, 670.0, 3478.5000000000005, 4917.849999999998, 7346.95, 0.33055611107346444, 1.3609614885602792, 0.11007776745708141], "isController": false}, {"data": ["Post Login", 200, 0, 0.0, 808.1550000000001, 212, 5294, 666.0, 1411.8, 2078.149999999999, 4998.240000000021, 0.32641390262094044, 0.15746920692846147, 0.13031055018695356], "isController": false}, {"data": ["Get Seller Product", 200, 0, 0.0, 3533.050000000002, 299, 12579, 3081.0, 6239.8, 9562.599999999993, 12267.380000000003, 0.32595745929198777, 0.8483170205206518, 0.10950133398090216], "isController": false}, {"data": ["Delete Seller Product by ID manual", 200, 200, 100.0, 1016.6550000000005, 144, 9576, 666.0, 2001.8, 3265.5499999999993, 8667.660000000025, 0.3286165898799235, 0.14762073373512188, 0.1177756723495429], "isController": false}, {"data": ["Get buyer Order by id order", 200, 0, 0.0, 1042.9549999999995, 62, 7492, 604.5, 2263.100000000001, 3330.7999999999993, 7476.3200000000115, 0.3307665846365537, 0.40505986048265463, 0.11208594225476966], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Median
            case 8:
            // Percentile 1
            case 9:
            // Percentile 2
            case 10:
            // Percentile 3
            case 11:
            // Throughput
            case 12:
            // Kbytes/s
            case 13:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": [{"data": ["400/Bad Request", 400, 66.66666666666667, 16.666666666666668], "isController": false}, {"data": ["404/Not Found", 200, 33.333333333333336, 8.333333333333334], "isController": false}]}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 2400, 600, "400/Bad Request", 400, "404/Not Found", 200, "", "", "", "", "", ""], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["Post  buyer Order", 200, 200, "400/Bad Request", 200, "", "", "", "", "", "", "", ""], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["Post Seller Product ", 200, 200, "400/Bad Request", 200, "", "", "", "", "", "", "", ""], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["Delete Seller Product by ID manual", 200, 200, "404/Not Found", 200, "", "", "", "", "", "", "", ""], "isController": false}, {"data": [], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
